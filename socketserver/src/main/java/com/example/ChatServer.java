package com.example;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class ChatServer extends Thread {

	Socket socket;
	BufferedWriter bw;
	BufferedReader br;

	public ChatServer(Socket socket) {
		this.socket = socket;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
			br = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void outPut(String str) {
		try {
			bw.write(str + "\n");
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		// int count = 0;
		// while(true) {
		// outPut("loop: " + count++);
		// try {
		// sleep(500);
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }
		// }

		try {
//			InputStream in = socket.getInputStream();
//			InputStreamReader ir = new InputStreamReader(in);
//			BufferedReader br = new BufferedReader(ir);
//			String line = null;
//			while ((line = br.readLine()) != null) {
//				ChatManager.getInstance().publish(this, line);
//				System.out.println(line);
//			}
//			br.close();
//			ir.close();
//			in.close();
			
			String line = null;  
            while ((line = br.readLine()) != null) {//监听客户端发来的数据  
                System.out.println("客户端发来数据："+line);  
                //把数据发给其余的客户端  
                ChatManager.getInstance().publish(this, line);  
            }  
            br.close();  

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
