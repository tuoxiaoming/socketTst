package com.mingotuo.socket;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private TextView content;
    private EditText address;
    private Button connect;
    private Button send;
    private EditText msg;
    private Socket socket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        content = (TextView) findViewById(R.id.content);
        address = (EditText) findViewById(R.id.edt);
        connect = (Button) findViewById(R.id.connect);
        msg = (EditText) findViewById(R.id.msg);
        send = (Button) findViewById(R.id.send);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                connectToServer();

            }

        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMsgToServer();
            }
        });


    }


    BufferedWriter bw;
    BufferedReader br;

    private void connectToServer() {
        AsyncTask<Void, String, Void> task = new AsyncTask<Void, String, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    socket = new Socket(address.getText().toString(), 12345);
                    bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                    br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    publishProgress("@success");

                } catch (IOException e) {
//                    Toast.makeText(MainActivity.this, "链接失败", Toast.LENGTH_SHORT).show();
                    publishProgress(e.getMessage());
                }

                try {
                    String line;
                    while ((line = br.readLine()) != null) {
                        publishProgress(line);
                    }
                } catch (IOException e) {
//                    Toast.makeText(MainActivity.this, "链接失败", Toast.LENGTH_SHORT).show();
                    publishProgress(e.getMessage());
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values[0].equals("@success")) {
                    Toast.makeText(MainActivity.this, "链接成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, values[0], Toast.LENGTH_SHORT).show();
                }
                content.append(values[0] + "\n");
//                super.onProgressUpdate(values);
            }
        };
        task.execute();
    }

    private  Handler handler = new Handler() {
         @Override
         public void handleMessage(Message message) {
             content.append(msg.getText().toString() + "\n");
             return ;
         }
     };

    private void sendMsgToServer() {

//        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... voids) {
//                if (bw != null) {
//                    try {
//                        bw.write(msg.getText().toString() + "\n");
//                        bw.flush();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void aVoid) {
//
//                content.append(msg.getText().toString() + "\n");
//                msg.setText("");
//                super.onPostExecute(aVoid);
//            }
//        };
//        task.execute();

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (bw != null) {
                    try {
                        bw.write(msg.getText().toString() + "\n");
                        bw.flush();
                        Message message = handler.obtainMessage();
                        message.what = 12345;
                        handler.sendMessage(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }
}
