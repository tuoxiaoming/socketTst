package com.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

public class MySocketServer extends Thread {

	@Override
	public void run() {

		try {
			ServerSocket socketServer = new ServerSocket(12345);

			while (true) {
				Socket socket = socketServer.accept();
				JOptionPane.showMessageDialog(null, "有客户端链接到12345端口");
//				new ChatServer(socket).start();

				ChatServer cs = new ChatServer(socket);
				cs.start();
				ChatManager.getInstance().add(cs);
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
