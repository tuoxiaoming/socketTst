package com.example;

import java.util.Vector;

public class ChatManager {

	private static ChatManager _isntance;

	private ChatManager() {

	}

	Vector<ChatServer> servers = new Vector<>();

	public static ChatManager getInstance() {
		if (_isntance == null) {
			synchronized (ChatManager.class) {
				if (_isntance == null) {
					_isntance = new ChatManager();
				}
			}
		}
		return _isntance;
	}

	public void add(ChatServer server) {//添加消息	
		servers.add(server);
	}

	public void publish(ChatServer cs, String msg) {//发布消息
		for (int i = 0; i < servers.size(); i++) {
			ChatServer chatServer = servers.get(i);
			if (!chatServer.equals(cs)) {
				chatServer.outPut(msg);
			}
		}
	}

}
